package com.bradsbrain.garage;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * The primary class in our application.
 * 
 * @author bradparks
 *
 */
public class GarageChecker {

    private static enum GarageDoorStatus {
        OPEN,
        CLOSED,
        UNKNOWN,
    }
    
    /*
     * The interval to wait between polling for changes
     */
    private int interval = 30;

    /**
     * Pretty basic constructor
     * 
     * @param interval
     */
    public GarageChecker(int interval) {
        this.interval = interval;
    }

    /**
     * The main method. This starts all the actions.
     * 
     * @param args - program arguments, probably blank
     */
    public static void main(String[] args) {
        GarageChecker checker = new GarageChecker(10);
        try {
            checker.run();
        } catch (InterruptedException e) {
            // intentionally ignoring exception
        }
    }

    /**
     * This method will have an infinite loop that polls for changes and does
     * something if the status changes
     * @throws InterruptedException 
     * 
     */
    private void run() throws InterruptedException {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:MM:ss");
        
        GarageDoorStatus previousDoorStatus = null;
        while (true) {
            GarageDoorStatus currentDoorStatus = getGarageDoorStatus();
            if (previousDoorStatus != currentDoorStatus) {
                sendNotification(currentDoorStatus); // send a notification whenever the status changes
                previousDoorStatus = currentDoorStatus;
            }
            System.out.println("Garage door status is " + currentDoorStatus + " at " + sdf.format(new Date(System.currentTimeMillis())));
            Thread.sleep(1000 * interval);  // wait
        }
        
    }

    /**
     * To be implemented later.
     * 
     * Send a notification to somebody.  Maybe this will be email or txt message.
     * 
     * @param currentDoorStatus - the current status
     */
    private void sendNotification(GarageDoorStatus currentDoorStatus) {
        System.out.println("Sending notification to somebody that the door status changed to: " + currentDoorStatus);
    }

    /**
     * To be implemented... probe for door status
     * 
     * @return the status of the door
     */
    private GarageDoorStatus getGarageDoorStatus() {
        return GarageDoorStatus.UNKNOWN;
    }

}
